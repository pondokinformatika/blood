@extends('admin.layouts.app')

@section('title', 'home')

@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Home
        <small>Halaman Home</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Dasboard</a></li>
        <li><a href="#">Home</a></li>
        <li class="active">Maps</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
        <a href="#" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah Pasien Baru</a>
      </div>
        <div class="box-header with-border">
          <div id="map">
            
          </div>
        </div>

        <!-- /.box-body -->

        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>

@endsection

@section('script')
  <script>
  $(document).ready(function () {
    $('.sidebar-menu').tree()
  })


  var map = L.map('map').setView([-7.78278, 110.36083], 13);

       L.tileLayer('http://{s}.tiles.wmflabs.org/bw-mapnik/{z}/{x}/{y}.png', {
        maxZoom: 18,
        attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
      }).addTo(map);


      L.marker([-7.797635, 110.370209]).addTo(map)
       .bindPopup('Lokasi Pasien <br><center><i class="fa fa-user"></i></center>')
       .openPopup();
      L.marker([-7.786773, 110.379573]).addTo(map)
       .bindPopup('Lokasi Pasien Dua <br><center><i class="fa fa-user"></i></center>')
       .openPopup();
      L.marker([-7.784800, 110.358341]).addTo(map)
       .bindPopup('Lokasi Pasien Eetiga <br><center><i class="fa fa-user"></i></center>')
       .openPopup();
      L.marker([-7.815992, 110.356862]).addTo(map)
       .bindPopup('Lokasi Pasien Empat <br><center><i class="fa fa-user"></i></center>')
       .openPopup();
      L.marker([-7.811858, 110.396555]).addTo(map)
       .bindPopup('Lokasi Pasien Lima <br><center><i class="fa fa-user"></i></center>')
       .openPopup();
</script>

@endsection