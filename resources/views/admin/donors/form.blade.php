<div class="col-md-9">
  <!-- Default box -->
  <div class="box">

    <div class="box-header with-border">
      <h3 class="box-title">{{ $donors->exists ? 'Edit' : 'Tambah' }} Donasi</h3>
      <span class="text-muted small pull-right">kolom yang memiliki tanda ( * ) wajib diisi!</span>
    </div>

    <!-- box-body -->
    <div class="box-body">
      <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
        {!! Form::label('Nama') !!}
        <span class="text-primary"> *<span>
        {!! Form::text('name', null,  ['class' => 'form-control', 'id' => 'name']) !!}

        @if ($errors->has('name'))
          <span class="help-block">{{ $errors->first('name') }}</span>
        @endif
      </div>

      <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
        {!! Form::label('Email') !!}
        <span class="text-primary"> *<span>
        {!! Form::email('email', null,  ['class' => 'form-control', 'id' => 'email']) !!}

        @if ($errors->has('email'))
          <span class="help-block">{{ $errors->first('email') }}</span>
        @endif
      </div>

      <div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
        {!! Form::label('Password') !!}
        <span class="text-primary"> *<span><br>
        {!! Form::password('password', null,  ['class' => 'form-control', 'id' => 'password']) !!}

        @if ($errors->has('password'))
          <span class="help-block">{{ $errors->first('password') }}</span>
        @endif
      </div>

      <div class="form-group {{ $errors->has('phone') ? 'has-error' : '' }}">
        {!! Form::label('No Hp') !!}
        <span class="text-primary"> *<span>
        {!! Form::number('phone', null,  ['class' => 'form-control', 'id' => 'phone']) !!}

        @if ($errors->has('phone'))
          <span class="help-block">{{ $errors->first('phone') }}</span>
        @endif
      </div>

      <div class="form-group {{ $errors->has('blod_type') ? 'has-error' : '' }}">
        {!! Form::label('Jenis Darah') !!}
        <span class="text-primary"> *<span>
        {!! Form::select('blod_type', ['A' => 'A', 'B' => 'B', 'AB' => 'AB', 'O' => 'O' ], null,  ['class' => 'form-control', 'id' => 'blod_type']) !!}

        @if ($errors->has('blod_type'))
          <span class="help-block">{{ $errors->first('blod_type') }}</span>
        @endif
      </div>

      <div class="form-group {{ $errors->has('province_id') ? 'has-error' : '' }}">
        {!! Form::label('Nama Provinsi') !!}
        <span class="text-primary"> *<span>
        {!! Form::number('province_id', null,  ['class' => 'form-control', 'id' => 'province_id']) !!}

        @if ($errors->has('province_id'))
          <span class="help-block">{{ $errors->first('province_id') }}</span>
        @endif
      </div>

      <div class="form-group {{ $errors->has('latitude') ? 'has-error' : '' }}">
        {!! Form::label('Latitud') !!}
        <span class="text-primary"> *<span>
        {!! Form::number('latitude', null,  ['class' => 'form-control', 'id' => 'latitude']) !!}

        @if ($errors->has('latitude'))
          <span class="help-block">{{ $errors->first('latitude') }}</span>
        @endif
      </div>

      <div class="form-group {{ $errors->has('longitude') ? 'has-error' : '' }}">
        {!! Form::label('Longitud') !!}
        <span class="text-primary"> *<span>
        {!! Form::number('longitude', null,  ['class' => 'form-control', 'id' => 'longitude']) !!}

        @if ($errors->has('longitude'))
          <span class="help-block">{{ $errors->first('longitude') }}</span>
        @endif
      </div>
 
    </div>
    <!-- /.box-body -->

    <div class="box-footer">
      <button type="submit" class="btn btn-primary" id="save-btn">{{ $donors->exists ? 'Perbarui' : 'Simpan' }}</button>
      <a href="{{ route('donors.index') }}" class="btn btn-default">Batal</a>
    </div>

  </div>
  <!-- /.box -->
</div>
