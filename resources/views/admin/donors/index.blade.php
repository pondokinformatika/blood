@extends('admin.layouts.app')

@section('title', 'Pendonor')

@section('content')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Pendonor
      <small>Mendata Pendonor</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
      <li><a href="{{ route('donors.index') }}">Pendonor</a></li>
      <li class="active">Daftar Pendonor</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="box">
      <div class="box-header with-border">
        <a href="{{ route('donors.create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah Baru</a>
      </div>
      <div class="box-body table-responsive">
        @include('admin.donors.table')
      </div>
      <!-- /.box-body -->
      <div class="box-footer">
        <div class="pull-left">
          {{ $donors->links() }}
        </div>
        <div class="item-count pull-right text-muted small">
          total {{ $itemCount." ".str_plural('item', $itemCount) }} 
        </div>
      </div>
    </div>
  </section>
  <!-- /.content -->
</div>

@endsection