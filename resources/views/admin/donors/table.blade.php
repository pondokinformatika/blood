<table class="table table-hover">
  
  <tr>
    <th width="40">No</th>
    <th>Nama</th>
    <th>No Hp</th>
    <th>Jenis Darah</th>
    <th>Tanggal</th>
    <th>action</th>
  </tr>
  <?php $no = 1; ?>
  @foreach($donors as $donor)
    <tr>
      <td>{{$no++}}</td>
      <td>{{$donor->name}}</td>
      <td>{{$donor->phone}}</td>
      <td>{{$donor->blod_type}}</td>
      <td>{{$donor->created_at}}</td>
      <td>
        <form action=" {{ route('donors.destroy', $donor->id) }} " method="post">
          {{ csrf_field() }}
          {{ method_field("DELETE") }}
          <a href="{{route('donors.edit', $donor->id)}}" class="btn btn-xs btn-primary">
            <i class="fa fa-edit"></i> Edit
          </a>
          <button onclick="return confirm('Are you sure?')" type="submit" class="btn btn-xs btn-danger">
            <i class="fa fa-trash"></i> Hapus
          </button>
        </form>
      </td>
    </tr>
  @endforeach

</table>