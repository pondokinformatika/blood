@extends('admin.layouts.app')

@section('title', 'Pasien')

@section('style')
<style type="text/css">
  .pagination {
    margin: 0px;
  }
  .item-count {
    margin-top: 5px;
  }
</style>
@endsection

@section('content')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Pasien
      <small>Mendata Pasien</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
      <li><a href="#}">Pasien</a></li>
      <li class="active">Daftar Pasien</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="box">
      <div class="box-header with-border">
        <a href="{{route('patients.create')}}" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah Baru</a>
      </div>
      <div class="box-body table-responsive">
        @include('admin.patients.table')
      </div>
      <!-- /.box-body -->
      <div class="box-footer">
        <div class="pull-left">
          {{ $patients->links() }}
        </div>
        <div class="item-count pull-right text-muted small">
          total {{ $itemCount." ".str_plural('item', $itemCount) }}
        </div>
      </div>
    </div>
  </section>
  <!-- /.content -->
</div>

@endsection