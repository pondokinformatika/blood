@extends('admin.layouts.app')

@section('title', 'Tambah Pasien Donor')

@section('content')
<div class="content-wrapper">

  <!-- Content Header (Page header) -->
  <section class="content-header">
     <h1>
      Pasien
      <small>Mendata Pasien Donor Darah</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{ route('home') }}"><i class="fa fa-home"></i> Home</a></li>
      <li><a href="{{ route('patients.index') }}">Pasien</a></li>
      <li class="active">Tambah Daftar Pasien</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">

      {!! Form::model($patient, [
        'method'  => 'POST',
        'route'   => 'patients.store',
        'id'      => 'patient-form'
      ]) !!}

      @include('admin.patients.form')

      {!! Form::close() !!}

    </div>

  </section>
  <!-- /.content -->
</div>

@endsection