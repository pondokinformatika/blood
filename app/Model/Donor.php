<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Donor extends Model
{
    protected $table = 'donors';
    protected $fillable = 
    [
    	'id', 'name', 'email', 
    	'password', 'phone', 'blod_type', 
    	'province_id', 'latitude', 'longitude', 
    	'timestamps'
    ];
}
