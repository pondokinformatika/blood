<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Province;
use App\Model\BloodType;
use App\Model\Patien;
use App\Http\Requests;

class PatientsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $patients = Patien::orderBy('created_at', 'desc')->paginate(8);
        $itemCount = Patien::count();

        return view('admin.patients.index', compact('patients', 'itemCount'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $patient = new Patien();
        $province = Province::orderBy('name', 'asc')->pluck('name', 'id')->toArray();
        $result = collect($province)->prepend('Silahkan Pilih', 0);
        $blood = BloodType::orderBy('name')->pluck('name', 'id')->toArray();
        return view('admin.patients.create', compact('patient', 'result', 'blood'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\PatientsStoreRequest $request)
    {
        Patien::create($request->all());

        return redirect('admin\patients');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $patient = Patien::findOrFail($id);
        $province = Province::orderBy('name', 'asc')->pluck('name', 'id')->toArray();
        $result = collect($province)->prepend('Silahkan Pilih', 0);
        $blood = BloodType::orderBy('name')->pluck('name', 'id')->toArray();
        return view('admin.patients.edit', compact('patient', 'result', 'blood'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\PatientsUpdateRequest $request, $id)
    {
        Patien::findOrFail($id)->update($request->all());

        return redirect('admin\patients');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Patien::findOrFail($id)->delete();
        return redirect('admin/patients');
    }
}
