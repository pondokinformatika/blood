<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('example');
});


Route::prefix('admin')->group(function () {
    Route::resource('/donors', 'Admin\DonorsController');
	Route::resource('/provinces', 'Admin\ProvincesController');
});

Route::get('/admin/home', [
	'uses'	=>	'Admin\HomeController@index',
	'as'	=>	'home'
]);
// Route::get('/admin/patients', [
// 	'uses'	=>	'Admin\PatientsController@index',
// 	'as'	=>	'patients'
// ]);

Route::prefix('admin')->group(function () {
	Route::resource('/patients', 'Admin\PatientsController');
});
